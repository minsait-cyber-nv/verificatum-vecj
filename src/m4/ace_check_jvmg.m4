
# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Elliptic Curve library (VEC). VEC
# is NOT free software. It is distributed under Verificatum License
# 1.0 and Verificatum License Appendix 1.0 for VEC.
#
# You should have agreed to this license and appendix when
# downloading VEC and received a copy of the license and appendix
# along with VEC. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VEC in any way and you must delete
# VEC immediately.

AC_DEFUN([ACE_CHECK_JVMG],[
AC_REQUIRE([ACE_PROG_JAVA])

AC_ARG_ENABLE([check_jvmg],
     [  --disable-check_jvmg    Skip checking that JVMG is installed.],
     [],[# Check for non-standard library we need.
ace_res=$($JAVA $JAVAFLAGS -classpath ../jvmg/jvmg.jar:$CLASSPATH:checks TestLoadJVMG)

echo -n "checking for jvmg.jar... "
if test "x$ace_res" = x;
then
   echo "yes"
else
   echo "no"
   AC_MSG_ERROR([$ace_res Please make sure that JVMG is installed (found at www.verificatum.com) and that your \$CLASSPATH points to the proper location. You can check your JVMG installation using 'java jvmg.Test'. This should give you a usage description. If you get a NoClassDefFoundError, then the proper jvmg.jar file can not be found. You may use the option --disable-check_jvmg to ignore this error if you know that you will install this package later.])
fi
])
])
