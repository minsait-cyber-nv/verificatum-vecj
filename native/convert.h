
/*
 * Copyright 2008-2016 Douglas Wikstrom
 *
 * This file is part of Verificatum Elliptic Curve library (VEC). VEC
 * is NOT free software. It is distributed under Verificatum License
 * 1.0 and Verificatum License Appendix 1.0 for VEC.
 *
 * You should have agreed to this license and appendix when
 * downloading VEC and received a copy of the license and appendix
 * along with VEC. If not, then the license and appendix are available
 * at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
 * http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
 *
 * If you do not agree to the combination of this license and
 * appendix, then you may not use VEC in any way and you must delete
 * VEC immediately.
 */

#include <jni.h>

#ifndef _convert
#define _convert
#ifdef __cplusplus
extern "C" {
#endif

/*
 * Translates the representation of a positive integer given as a
 * jbyteArray in two's complement representation into its
 * representation as a GMP mpz_t element. It initializes gmpValue, so
 * it should point to an uninitialized variable before the call.
 */
void
jbyteArray_to_mpz_t(JNIEnv* env, mpz_t* gmpValue, jbyteArray javaBytes);

/*
 * Translates the representation of a positive integer given as a GMP
 * mpz_t element into its representation as a two's complement in a
 * jbyteArray. It allocates a jbyteArray in JVM memory space, so it
 * should be uninitialized before the call.
 */
void
mpz_t_to_jbyteArray(JNIEnv* env, jbyteArray* javaBytes, mpz_t gmpValue);

#ifdef __cplusplus
}
#endif
#endif
