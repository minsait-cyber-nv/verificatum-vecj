
# Copyright 2008-2016 Douglas Wikstrom
#
# This file is part of Verificatum Elliptic Curve library (VEC). VEC
# is NOT free software. It is distributed under Verificatum License
# 1.0 and Verificatum License Appendix 1.0 for VEC.
#
# You should have agreed to this license and appendix when
# downloading VEC and received a copy of the license and appendix
# along with VEC. If not, then the license and appendix are available
# at: http://www.verificatum.com/VERIFICATUM_LICENSE_1.0
# http://www.verificatum.com/VERIFICATUM_LICENSE_1.0_APPENDIX_VEC
#
# If you do not agree to the combination of this license and
# appendix, then you may not use VEC in any way and you must delete
# VEC immediately.

ACLOCAL_AMFLAGS=-I m4

# Extracts the flags used by GMP.
GMP_CFLAGS := $(shell ./extract_GMP_CFLAGS)

# We use pedantic flags, strip the optimization flag of GMP, and
# insert our own level of optimization.
AM_CFLAGS := -O3 -Wall -W -Werror $(shell echo ${GMP_CFLAGS} | sed -e "s/-O[O12345]//")

libvecj_la_LIBADD = -lgmp -lvec

# We use -release to glue the native code and Java code together. We
# are aware that this violate common practice for library versioning.
libvecj_la_LDFLAGS = -release $(VERSION)

# This is generated from a Java file and copied to this directory by
# the parent directory.
# include_HEADERS = com_verificatum_vecj_VEC.h

lib_LTLIBRARIES = libvecj.la
libvecj_la_SOURCES = com_verificatum_vecj_VEC.c convert.c convert.h

dist_noinst_DATA = extract_GMP_CFLAGS.c

clean-local:
	find . -name "*~" -delete
	rm -rf compile
